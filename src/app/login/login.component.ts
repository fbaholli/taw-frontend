import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  title = 'Sign in';
  registerText = 'New User? Register!'
  loginForm = new FormGroup({
    username: new FormControl('', {
      validators: [Validators.required, Validators.minLength(3)],
      updateOn: 'change',
    }),
    password: new FormControl('', {
      validators: [Validators.required, Validators.minLength(4)],
      updateOn: 'change',
    }),
    remember: new FormControl(false),
  });

  constructor(private authService: AuthService, private router: Router) { }

  async ngOnInit() {
    const loggedIn = await this.authService.isTokenExpired(
      await this.authService.getAccessToken()
    );

    if (!loggedIn) this.router.navigateByUrl('/dashboard');
  }

  login() {
    const username: any = this.loginForm.get('username')?.value;
    const password: any = this.loginForm.get('password')?.value;
    return this.authService
      .login({ username: username, password })
      .then((res: any) => {
        this.authService.setAccessToken(res.accessToken);
        this.router.navigateByUrl('/clients');
      });
  }
}
