import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(public http: HttpClient, private router: Router) {}

  login(credentials: any): any {
    return this.http
      .post(environment.url + '/user/login', credentials)
      .toPromise();
  }

  // this.authService.getAccessToken() &&
  // !this.authService.isTokenExpired(this.authService.getAccessToken())

  getAccessToken(): string | null {
    const accessToken = localStorage.getItem('accessToken');
    return accessToken ? accessToken : null;
  }

  setAccessToken(token: string) {
    return localStorage.setItem('accessToken', token);
  }

  isTokenExpired(token: string | null): boolean {
    if (token) {
      const expiry = this.getDecodedToken(token).exp;
      return Math.floor(new Date().getTime() / 1000) >= expiry;
    }
    return false;
  }

  getDecodedToken(token: string): any {
    return jwt_decode(token);
  }

  logout() {
    this.router.navigateByUrl('/login');
    localStorage.removeItem('accessToken');
    localStorage.clear();
  }

  isNotTestUser(token: string) {}

  getRole() {
    let token = this.getAccessToken();
    if (token) {
      let decodedToken = this.getDecodedToken(token);
      return decodedToken.role;
    } else return false;
  }
}
